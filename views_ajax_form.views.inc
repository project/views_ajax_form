<?php

/**
 * @file
 * Contains hook implementations for the Views integration.
 */

/**
 * Implements hook_views_plugins().
 */
function views_ajax_form_views_plugins() {
  $plugins['exposed_form']['views_ajax_form'] = array(
    'title' => t('Asynchronous loading'),
    'help' => t('An exposed form that will be loaded asynchronously from the user browser.'),
    'handler' => 'ViewsAjaxForm',
    'uses options' => TRUE,
  );
  return $plugins;
}
