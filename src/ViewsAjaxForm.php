<?php

/**
 * Provides an exposed form that will be loaded asynchronously.
 *
 * @see views_ajax_form_views_plugins()
 */
class ViewsAjaxForm extends views_plugin_exposed_form_input_required {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['input_required'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['input_required'] = array(
      '#type' => 'checkbox',
      '#title' => t('Input required'),
      '#description' => t('Only display results if there was any user input. (Emulates the behavior of the "Input required" exposed form style.)'),
      '#default_value' => $this->options['input_required'],
    );
    $form['text_input_required']['#weight'] = 1;
    $form['text_input_required']['#dependency'] = array(
      'edit-exposed-form-options-input-required' => array(TRUE),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function exposed_filter_applied() {
    // Always return TRUE if "Input required" is not enabled.
    if (!$this->options['input_required']) {
      return TRUE;
    }

    // Ignore empty input.
    foreach ($this->view->exposed_input as $key => $value) {
      if (is_array($value)) {
        if (!array_filter($value)) {
          unset($this->view->exposed_input[$key]);
        }
      }
      elseif ("$value" === '') {
        unset($this->view->exposed_input[$key]);
      }
    }

    return parent::exposed_filter_applied();
  }

  /**
   * {@inheritdoc}
   */
  public function render_exposed_form($block = FALSE) {
    $display = $this->view->display_handler;
    // If no exposed form will be shown.
    if (!$display->displays_exposed() || (!$block && $display->get_option('exposed_block'))) {
      return '';
    }

    // Build the form state (taken from parent method).
    $form_state = array(
      'view' => $this->view,
      'display' => $this->display,
      'exposed_form_plugin' => $this,
      'method' => 'get',
      'rerender' => TRUE,
      'no_redirect' => TRUE,
      'always_process' => TRUE,
      'ajax' => !empty($this->ajax),
    );

    // Cache the form state to be able to use it in the AJAX request. One minute
    // should be plenty to allow for the AJAX request.
    $id = uniqid();
    cache_set($id, $form_state, 'cache_views_ajax_form', REQUEST_TIME + 60);

    // Add the necessary Javascript file to the page and return a placeholder
    // for the form.
    drupal_add_library('system', 'drupal.ajax');
    drupal_add_js(drupal_get_path('module', 'views_ajax_form') . '/views_ajax_form.js');
    return "<span class=\"views-ajax-form-placeholder show-progress\" id=\"views-ajax-form-placeholder-$id\" data-form-id=\"$id\"></span>";
  }

}
