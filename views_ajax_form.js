/**
 * @file
 * Contains Javascript behaviors for the Views AJAX Forms module.
 */

(function ($) {

/**
 * Triggers an AJAX load of the exposed form for all placeholders present.
 */
Drupal.behaviors.viewsAjaxForm = {
  attach: function (context) {
    var event = 'viewsAjaxForm';
    $(context).find('.views-ajax-form-placeholder').each(function () {
      var $this = $(this);
      var base = $this.attr('id');
      var element_settings = {
        url: '/views-ajax-form/' + $this.data('form-id'),
        event: event,
        keypress: false,
        selector: '#' + base,
        effect: 'none',
        speed: 'none',
        method: 'replaceWith',
        progress: {
          type: $this.hasClass('show-progress') ? 'throbber' : 'none',
        },
        submit: {
          'js': true,
        },
      };
      new Drupal.ajax(base, this, element_settings);
      $this.trigger(event);
      $this.unbind(event);
    });
  }
};

})(jQuery);
